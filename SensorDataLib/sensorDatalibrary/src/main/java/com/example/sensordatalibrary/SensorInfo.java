package com.example.sensordatalibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class SensorInfo implements Parcelable {
    private float pitch;
    private float roll;

    public SensorInfo(float pitch,  float  roll) {
        this.pitch = pitch;
        this.roll = roll;

    }

    private SensorInfo(Parcel in) {
        pitch = in.readFloat();
        roll = in.readFloat();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(pitch);
        out.writeDouble(roll);

    }

    public static final Parcelable.Creator<SensorInfo> CREATOR
            = new Parcelable.Creator<SensorInfo>() {
        public SensorInfo createFromParcel(Parcel in) {
            return new SensorInfo(in);
        }

        public SensorInfo[] newArray(int size) {
            return new SensorInfo[size];
        }
    };


    public float getPitch() {
        return pitch;
    }
    public float getRoll() {
        return roll;
    }

    @Override
    public String toString() {
        return "SensorInfo{" +
                "pitch=" + pitch +
                ", roll=" + roll +
                '}';
    }
}

package com.example.sensordatalibrary;

import android.app.Application;
import android.content.Context;

public class SensorApplicationClass extends Application {
    private static Context appContext = getAppContext();
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }
    public static Context getAppContext() {
        return appContext;
    }
}

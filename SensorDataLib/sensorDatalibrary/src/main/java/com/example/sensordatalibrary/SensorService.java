package com.example.sensordatalibrary;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;




public class SensorService extends Service implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mRotationSensor;
    List<SensorInfo> sensorInfoList = Collections.synchronizedList(new ArrayList<SensorInfo>());

    private static final int SENSOR_DELAY =  8000; // 500ms
    private static final int FROM_RADS_TO_DEGS = -57;
    @Override
    public void onCreate() {
        super.onCreate();
        mSensorManager = (SensorManager) SensorApplicationClass.getAppContext().getSystemService(Activity.SENSOR_SERVICE);
        mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mSensorManager.registerListener(this, mRotationSensor, SENSOR_DELAY);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return mBinder;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor == mRotationSensor) {
            if (event.values.length > 4) {
                float[] truncatedRotationVector = new float[4];
                System.arraycopy(event.values, 0, truncatedRotationVector, 0, 4);
                update(truncatedRotationVector);
            } else {
                update(event.values);
            }
        }
    }

    public List<SensorInfo> update(float[] vectors) {

        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, vectors);
        int worldAxisX = SensorManager.AXIS_X;
        int worldAxisZ = SensorManager.AXIS_Z;
        float[] adjustedRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisX, worldAxisZ, adjustedRotationMatrix);
        float[] orientation = new float[3];
        SensorManager.getOrientation(adjustedRotationMatrix, orientation);
        float pitch = orientation[1] * FROM_RADS_TO_DEGS;
        float roll = orientation[2] * FROM_RADS_TO_DEGS;
        SensorInfo sensorInfo1 = new SensorInfo(pitch,roll);
        Log.d("Pitch","Pitch ::"+ pitch);
        Log.d("roll","roll ::"+ roll);
      sensorInfoList.add(sensorInfo1);
        return sensorInfoList;
    }
    private final ISensorInterface.Stub mBinder = new ISensorInterface.Stub() {

        @Override
        public List<SensorInfo> getSensorData() throws RemoteException {
            return sensorInfoList;
        }
    };
}

// ISensorInterface.aidl
package com.example.sensordatalibrary;

// Declare any non-default types here with import statements
parcelable SensorInfo;
interface ISensorInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
     List<SensorInfo> getSensorData();
}

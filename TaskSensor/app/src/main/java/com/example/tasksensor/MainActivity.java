package com.example.tasksensor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sensordatalibrary.ISensorInterface;
import com.example.sensordatalibrary.SensorInfo;
import java.util.List;

public class MainActivity extends AppCompatActivity  {
    private ISensorInterface service;
    private RemoteServiceConnection serviceConnection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectService();
    }

    private void connectService() {
        serviceConnection = new RemoteServiceConnection();

        Intent intent = new Intent(MainActivity.this, com.example.sensordatalibrary.SensorService.class);
        intent.setAction(ISensorInterface.class.getName());
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
    class RemoteServiceConnection implements ServiceConnection {

        public void onServiceConnected(ComponentName name, IBinder boundService) {
            service = ISensorInterface.Stub.asInterface((IBinder) boundService);
            Toast.makeText(MainActivity.this, "Service connected", Toast.LENGTH_LONG)
                    .show();
            try {
                if (service != null) {
                    // String name = ((EditText) findViewById(R.id.edtSearchProduct)).getText().toString();
                    // Product product = service.getProduct(name);
                    List<SensorInfo> sensorInfoList = service.getSensorData();
                    Log.d("Sensorcount ",""+sensorInfoList.size());
                    for (int i=0;i<sensorInfoList.size();i++){
                        ((TextView) findViewById(R.id.pitch)).setText("Pitch in act:"+sensorInfoList.get(i).getPitch());
                        ((TextView) findViewById(R.id.roll)).setText("Roll in act:"+sensorInfoList.get(i).getRoll());
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Service is not connected", Toast.LENGTH_LONG)
                            .show();
                }
            } catch(Exception e) {
                Log.d("exception",""+e.getMessage());
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            service = null;
            Toast.makeText(MainActivity.this, "Service disconnected", Toast.LENGTH_LONG)
                    .show();
        }
    }
}
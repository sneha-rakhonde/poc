package com.example.homelauncher;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.launcherlibrary.PInfo;

import java.util.ArrayList;
import java.util.List;

public class LauncherAdapter extends RecyclerView.Adapter<LauncherAdapter.ViewHolder> implements Filterable {
        private ArrayList<PInfo> pinfos;
    private ArrayList<PInfo> pinfosFull;

        public LauncherAdapter(ArrayList<PInfo> pinfos) {
            this.pinfos = pinfos;
            pinfosFull = new ArrayList<>(pinfos);
        }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView applicationName;
            public TextView applicationVersionName;
            public TextView applicationVersionCode;
            public TextView packageName;
            public ImageView appIcon;


            //This is the subclass ViewHolder which simply
            //'holds the views' for us to show on each row
            public ViewHolder(View itemView) {
                super(itemView);

                //Finds the views from our row.xml
                appIcon = itemView.findViewById(R.id.appImage);
                applicationName = itemView.findViewById(R.id.appName);
                applicationVersionName = itemView.findViewById(R.id.versionName);
                applicationVersionCode = itemView.findViewById(R.id.VersionCode);
                packageName =  itemView.findViewById(R.id.packageName);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick (View v) {
                int pos = getAdapterPosition();
                Context context = v.getContext();

                Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(pinfos.get(pos).pname);
                Toast.makeText(v.getContext(), pinfos.get(pos).icon.toString(), Toast.LENGTH_LONG).show();
                if (launchIntent != null) {
                    context.startActivity(launchIntent);
                } else {
                    Toast.makeText(context, "Package not found", Toast.LENGTH_SHORT).show();
                }

            }
        }

        @Override
        public void onBindViewHolder(LauncherAdapter.ViewHolder viewHolder, int i) {

            //Here we use the information in the list we created to define the views


            String appName = pinfos.get(i).appname;
            String VersionCode = ""+pinfos.get(i).versionCode;
            String VersionName = pinfos.get(i).versionName;
            String pacName = pinfos.get(i).pname;;


            Drawable appIcon = pinfos.get(i).icon;

            TextView appNameTxt = viewHolder.applicationName;
            appNameTxt.setText(appName);

            TextView VersionCodeTxt = viewHolder.applicationVersionCode;
            VersionCodeTxt.setText(VersionCode);

            TextView VersionNameTxt = viewHolder.applicationVersionName;
            VersionNameTxt.setText(VersionName);

            TextView pacNameTxt = viewHolder.packageName;
            pacNameTxt.setText(pacName);

            ImageView imageView = viewHolder.appIcon;
            imageView.setImageDrawable(appIcon);
        }


        @Override
        public int getItemCount() {

            //This method needs to be overridden so that Androids knows how many items
            //will be making it into the list

            return pinfos.size();
        }


        @Override
        public LauncherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            //This is what adds the code we've written in here to our target view
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            View view = inflater.inflate(R.layout.row, parent, false);

            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }
    @Override
    public Filter getFilter() {
        return launcherFilter;
    }
    private Filter launcherFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PInfo> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(pinfosFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (PInfo item : pinfosFull) {
                    if (item.appname.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            pinfos.clear();
            pinfos.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}




package com.example.launcherlibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class AppReceiver extends BroadcastReceiver {

    static Context context;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent);
        asyncTask.execute();
    }
    private static class Task extends AsyncTask<String, Integer, String> {

        private final PendingResult pendingResult;
        private final Intent intent;

        private Task(PendingResult pendingResult, Intent intent) {
            this.pendingResult = pendingResult;
            this.intent = intent;
        }

        @Override
        protected String doInBackground(String... strings) {
            StringBuilder sb = new StringBuilder();
            sb.append("Action: " + intent.getAction() + "\n");
            sb.append("URI: " + intent.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n");
            String log = sb.toString();

            if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                Log.d(" BroadcastReceiver ", "onReceive called "
                        + " PACKAGE_REMOVED ");
                Toast.makeText(context, " onReceive !!!! PACKAGE_REMOVED",
                        Toast.LENGTH_LONG).show();

            }
            // when package installed
            else if (intent.getAction().equals(
                    "android.intent.action.PACKAGE_ADDED")) {

                Log.d(" BroadcastReceiver ", "onReceive called " + "PACKAGE_ADDED");
                Toast.makeText(context, " onReceive !!!!." + "PACKAGE_ADDED",
                        Toast.LENGTH_LONG).show();

            }
            //Log.d(TAG, log);
            return log;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish();
        }
    }
}

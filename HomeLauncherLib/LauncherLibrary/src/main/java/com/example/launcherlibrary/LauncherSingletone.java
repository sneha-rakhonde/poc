package com.example.launcherlibrary;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;

public class LauncherSingletone extends Application {

        private static Context appContext = getAppContext();
        @Override
        public void onCreate() {
            super.onCreate();
            appContext = this;
        }
        public static Context getAppContext() {
            return appContext;
        }
}

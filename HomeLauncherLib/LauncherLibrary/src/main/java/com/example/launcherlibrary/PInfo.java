package com.example.launcherlibrary;

import android.graphics.drawable.Drawable;
import android.util.Log;

import java.util.Comparator;

public class PInfo  {
    public String appname = "";
    public String pname = "";
    public String versionName = "";
    public int versionCode = 0;
    public Drawable icon;

    public static Comparator<PInfo> pinfoComparator = new Comparator<PInfo>() {

        public int compare(PInfo s1, PInfo s2) {
            String appname1 = s1.appname;
            String appname2 = s2.appname;

            //ascending order
            return appname1.compareTo(appname2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };

}

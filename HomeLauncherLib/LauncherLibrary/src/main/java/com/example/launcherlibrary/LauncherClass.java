package com.example.launcherlibrary;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LauncherClass
{

    public ArrayList<PInfo> getInstalledApps(boolean getSysPackages) {
        ArrayList<PInfo> res = new ArrayList<PInfo>();

       // List<PackageInfo> packs = LauncherSingletone.getAppContext().getPackageManager().getInstalledPackages(0);
        List<PackageInfo> packs = LauncherSingletone.getAppContext().getPackageManager().getInstalledPackages(0);
        if(packs.size() >0 )
        {
            for(int i=0;i<packs.size();i++) {
                PackageInfo p = packs.get(i);
                if ((!getSysPackages) && (p.versionName == null)) {
                    continue ;
                }
                PInfo newInfo = new PInfo();
                newInfo.appname = p.applicationInfo.loadLabel(LauncherSingletone.getAppContext().getPackageManager()).toString();
                //  Log.d("app name ::", newInfo.appname);
                newInfo.pname = p.packageName;
                newInfo.versionName = p.versionName;
                newInfo.versionCode = p.versionCode;
                newInfo.icon = p.applicationInfo.loadIcon(LauncherSingletone.getAppContext().getPackageManager());
                res.add(newInfo);
            }
            Collections.sort(res, PInfo.pinfoComparator);

            for(PInfo str: res){
                Log.d("app name ::", str.appname);
            }


        }

        return res;
    }
}



